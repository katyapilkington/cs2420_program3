public class GameNode {
    GameNode previousNode = null; // Keep track of the node that comes before.
    GameState gameState; // The board that the current node contains.
    GameNode nextNode = null; // Keep track of the node that comes after.

    // Node constructor.
    GameNode(GameState gameState){ this.gameState = gameState;}
}
