// Game state carries information important to playing SliderGame with boards using aStarSolve.

public class GameState implements Comparable<GameState>{
    private Board board = new Board();
    private int costSoFar;   // How many moves already made to get to current board.
    private int remainingCost = estimateRemainingManhattan(); // Lower-bound estimate for remaining moves until solved
    private int priority; // costSoFar + remainingCost; how boards are to be compared for priority queue.


    // Constructor given just a board and costSoFar.
    public GameState(Board board, int costSoFar) {
        this.board = new Board(board);
        this.costSoFar = costSoFar;
        priority = costSoFar + remainingCost;
    }

    // Copy constructor
    public GameState(GameState gameState){
        this.board = new Board(gameState.getBoard());
        this.costSoFar = gameState.getCostSoFar();
        priority = costSoFar + remainingCost;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public int getCostSoFar() {
        return costSoFar;
    }

    public void setCostSoFar(int costSoFar) {
        this.costSoFar = costSoFar;
        priority = costSoFar + remainingCost;
    }

    // Recalculate remainingCost and update GameState priority.
    public void updatePriority(){
        remainingCost = estimateRemainingManhattan();
        priority = costSoFar + remainingCost;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }


    // Give a lower-bound estimate on the number of moves necessary to solve the board. Find the number the farthest
    // distance from where it should be and return that distance.
    private int estimateRemainingManhattan(){

        int maxManhattan = 0;
        int rowWanted;
        int columnWanted;

        // Iterate over each number in board, calculating distance from current place to desired place. Update maxManhattan
        // when a larger distance is found.
        for (int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){

                int spaceValue = board.getBoard()[i][j];

                if (spaceValue == 0){
                    rowWanted = 2;
                    columnWanted = 2;
                }
                // Calculate what the column and row should be based on the given value on the board at [i][j]
                else{
                    rowWanted = (spaceValue - 1) / 3;
                    columnWanted = (spaceValue - 1) % 3;
                }

                // Calculate Manhattan Distance based on difference between wanted and actual rows and columns.
                int manhattanDistance = Math.abs(rowWanted - i) + Math.abs(columnWanted - j);

                // Find the largest manhattanDistance to return.
                if (manhattanDistance > maxManhattan){
                    maxManhattan = manhattanDistance;
                }
            }
        }
        return maxManhattan;
    }

    // Override compareTo function to compare priorities.
    public int compareTo(GameState state2){
        if (this.priority > state2.priority){
            return 1;
        }
        if (this.priority == state2.priority){
            return 0;
        }
        return -1;
    }

}
