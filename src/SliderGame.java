// SliderGame can use slider board games using two methods bruteForceSolve (try all boards in order created), or
// aStarSolve, which uses a priority queue to try the "best" board first.
public class SliderGame {
    final Board finalBoard = makeFinalBoard();

    public Board makeFinalBoard() {
        int[] finalValues = {1, 2, 3, 4, 5, 6, 7, 8, 0};
        Board finalBoard = new Board();
        finalBoard.makeBoard(finalValues); // Construct what the solved board should match.
        return finalBoard;
    }

    public int bruteForceSolve(Board board){
        // Make sure the given board is not already solved
        if(board.equals(finalBoard)) {
            System.out.print("Board already solved: no moves necessary");
            return 0;
        }

        BoardQueue boardQueue = new BoardQueue(board); // Initialize queue with given board as head node.

        boolean solved = false;
        char[] movesArray = {'U', 'D', 'L', 'R'};
        while(!solved){ // Continue trying new moves/boards until the puzzle is solved.
            if(boardQueue.getCurrentBoard() == null){ // If the queue runs out without finding a solution, there are no
                printGame(board, "No Solution");               // solutions to the board.
                System.out.println("Total Number of Boards: " + boardQueue.getQueueLength());
                return boardQueue.getQueueLength();
            }
            Board currentBoard = boardQueue.getCurrentBoard();
            String moves = currentBoard.getMoves();
            char lastMove;
            if(moves == null || moves.length() == 0){
                lastMove = 0;
            }
            else{
                // Not the most elegant solution, but once the number of moves gets this big, it is pretty clear that
                // there is not a solution to the given board (aka Java returns a memory overfull error).
                if(moves.length() >= 27){
                    printGame(board, "No Solution");
                    System.out.println("Total Number of Boards: " + boardQueue.getQueueLength());
                    return boardQueue.getQueueLength();
                }
                // Get the last move so that you don't try a move that undoes the previous move.
                lastMove = moves.charAt(moves.length()-1);
            }
            for(char c : movesArray){ // Try each move for the current board.
                Board newBoard = new Board(currentBoard);
                char newMove = newBoard.makeMove(c, lastMove);
                if(newMove != ' '){
                    newBoard.addMove(newMove); // If the new move worked, add the resulting board to the queue
                    boardQueue.addNode(newBoard);
                    if(newBoard.equals(finalBoard)) { // Check to see if the resulting board matches the solution
                        System.out.println("Method Used: Brute Force");

                        printGame(board, newBoard.getMoves());
                        // Get queue information
                        System.out.println("Number of Boards Tried: " + boardQueue.getNumberDequeued());
                        solved = true;
                    }
                }
            }
            boardQueue.moveNext(); // None of the moves created the solution --> move on to the next board in the queue.
        }
        return boardQueue.getNumberDequeued();
    }

    public int aStarSolve(GameState gameState){

        // Make sure the given board is not already solved
        if(gameState.getBoard().equals(finalBoard)) {
            System.out.print("Board already solved: no moves necessary");
            return 0;
        }

        AVLTree<GameState> priorityQueue = new AVLTree<>(); // Make a priority queue using an AVLTree structure.
        priorityQueue.insert(gameState);
        int statesPulledOff = 0;

        boolean solved = false;
        char[] movesArray = {'U', 'D', 'L', 'R'};
        while(!solved){ // Continue trying new moves/boards until the puzzle is solved.
            if(priorityQueue.isEmpty()){ // If the queue runs out without finding a solution, there are no
                printGame(gameState.getBoard(), "No Solution"); // solutions to the board.
                return statesPulledOff;
            }

            // Delete the minimum node off queue to try.
            priorityQueue.deleteMin();
            GameState bestState = priorityQueue.getMinimumValue();

            statesPulledOff = statesPulledOff + 1;
            Board currentBoard = bestState.getBoard();

            String moves = currentBoard.getMoves();
            char lastMove;
            if(moves == null || moves.length() == 0){
                lastMove = 0;
            }
            else{
                // If the queue ends up empty, all possible boards were tried and there is no solution.
                if(priorityQueue.isEmpty()){
                    printGame(gameState.getBoard(), "No Solution");
                    System.out.println("Number of Board Tried " + statesPulledOff);
                    return statesPulledOff;
                }
                // Get the last move so that you don't try a move that undoes the previous move.
                lastMove = moves.charAt(moves.length()-1);
            }
            for(char c : movesArray){ // Try each move for the current board.
                GameState newState = new GameState(bestState);
                newState.setCostSoFar(newState.getCostSoFar() + 1);
                char newMove = newState.getBoard().makeMove(c, lastMove);
                if(newMove != ' '){
                    newState.getBoard().addMove(newMove); // If the new move worked, add the resulting board to the queue
                    newState.updatePriority();
                    priorityQueue.insert(newState);

                    if(newState.getBoard().equals(finalBoard)) { // Check to see if the resulting board matches the solution

                        System.out.println("Method Used: A-Star Solve");

                        printGame(gameState.getBoard(), newState.getBoard().getMoves());

                        // Get queue information
                        System.out.println("Number of Boards Tested: " + statesPulledOff);

                        solved = true;
                    }
                }
            }
        }
        // return the number of boards tried and deleted off queue
        return statesPulledOff;
    }

    // Print the game based on a board and its solution in string form. If there is a solution print each of the moves
    // and resulting boards, then the board solved and number of moves required to solve.
    public void printGame(Board board, String solution) {
        if(solution.equals("No Solution")){
            System.out.println(solution);
            return;
        }
        System.out.println("Show Me\nSolution:" + solution);
        char lastMove = 0;
        for(char c : solution.toCharArray()){
            System.out.println(board + "Next Move: " + c + "==>" + "\n");
            board.makeMove(c, lastMove);
            lastMove = c;
        }
        System.out.println("Solved:\n" + board + "\nMoves Required: " + solution.length());

    }

    public static void main(String[] args) {
        // Playing games 1-4 from given boards. Solve using bruteForceSolve and aStarSolve, then print the ratio of
        // number of boards tried for each one.
        SliderGame game1 = new SliderGame();
        int [] values1 = { 4, 0, 1, 3, 5, 2, 6, 8, 7};
        Board board1 = new Board();
        board1.makeBoard(values1);
        GameState gameState1 = new GameState(board1, 0);
        int bruteForce = game1.bruteForceSolve(board1);
        int aStar = game1.aStarSolve(gameState1);
        System.out.println("Ratio of Brute Force to A Star: " + ((double)bruteForce / aStar));
        System.out.println();


        SliderGame game2 = new SliderGame();
        int[] values2 = { 1, 3, 8, 6, 2, 0, 5, 4, 7 };
        Board board2 = new Board();
        board2.makeBoard(values2);
        GameState gameState2 = new GameState(board2, 0);
        bruteForce = game2.bruteForceSolve(board2);
        aStar = game2.aStarSolve(gameState2);
        System.out.println("Ratio of Brute Force to A Star: " + ((double)bruteForce / aStar));
        System.out.println();


        SliderGame game3 = new SliderGame();
        int[] values3 = {1, 3, 2, 4, 5, 6, 8, 7, 0};
        Board board3 = new Board();
        board3.makeBoard(values3);
        GameState gameState3 = new GameState(board3, 0);
        bruteForce = game3.bruteForceSolve(board3);
        aStar = game3.aStarSolve(gameState3);
        System.out.println("Ratio of Brute Force to A Star: " + ((double)bruteForce / aStar));
        System.out.println();

        SliderGame game4 = new SliderGame();
        int[] values4 = {1, 2, 3, 7, 4, 0, 6, 5, 8};
        Board board4 = new Board();
        board4.makeBoard(values4);
        GameState gameState4 = new GameState(board4, 0);
        bruteForce = game4.bruteForceSolve(board4);
        aStar = game4.aStarSolve(gameState4);
        System.out.println("Ratio of Brute Force to A Star: " + ((double)bruteForce / aStar));
        System.out.println();

        // game5 comes from jumbling a board 27 times. Shows that aStarSolve much faster than bruteForceSolve.
        SliderGame game5 = new SliderGame();
        int[] values5 = {7, 1, 4, 0, 5, 3, 6, 2, 8};
        Board board5 = new Board();
        board5.makeBoard(values5);
        GameState gameState5 = new GameState(board5, 0);
        bruteForce = game5.bruteForceSolve(board5);
        aStar = game5.aStarSolve(gameState5);
        System.out.println("Ratio of Brute Force to A Star: " + ((double)bruteForce / aStar));
        System.out.println();

    }

}
