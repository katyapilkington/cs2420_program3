public class BoardQueue {
    BoardNode head; //Keep track of the first node in the linked list.
    BoardNode tail; //Keep track of the last node in the linked list.
    BoardNode currentNode; //Instead of deleting nodes off the linked list, keep track of a pointer to the current node.
    int numberDequeued = 0;

    // Ceate a queue based on an initial board.
    BoardQueue(Board headBoard){
        this.head = new BoardNode(headBoard);
        tail = head;
        currentNode = head;
    }


    public int getNumberDequeued() {
        return numberDequeued;
    }

    // Add a board node to the end of the queue.
    public void addNode(Board b){
        BoardNode newNode = new BoardNode(b);
        tail.nextNode = newNode;
        newNode.previousNode = tail;
        tail = newNode;
    }

    public boolean next(){
        if(currentNode.nextNode != null){
            currentNode = currentNode.nextNode;
            return true;
        }
        return false;
    }

    public void moveNext(){
        currentNode = currentNode.nextNode;
        numberDequeued = numberDequeued + 1;
    } // Move the pointer to te next node.

    public Board getCurrentBoard(){
        return currentNode.board;
    } // Return the board of the current node.

    // Return the length of the queue starting at a given node to the tail of the queue.
    public int getQueueLength(BoardNode beginning){
        if(beginning == null){
            return 0;
        }
        BoardNode current = beginning;
        int length = 1;
        while(current.nextNode != null){
            current = current.nextNode;
            length += 1;
        }
        return length;
    }
    // Return the length of the queue from head to tail.
    public int getQueueLength(){
        return getQueueLength(head);
    }
}