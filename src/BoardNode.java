public class BoardNode {
    BoardNode previousNode = null; // Keep track of the node that comes before.
    Board board; // The board that the current node contains.
    BoardNode nextNode = null; // Keep track of the node that comes after.

    // Node constructor.
    BoardNode(Board b){
        board = b;
    }
}

