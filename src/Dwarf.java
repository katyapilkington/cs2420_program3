public class  Dwarf implements Comparable<Dwarf>{
    String data;
    int which;
   static  int count = 0;
    public Dwarf(String name){
        data = name;
        which= count++;
    }
    @Override
    public int compareTo(Dwarf dwarf2){
        return (this.data.compareTo( dwarf2.data));
    }
    public String toString(){
        return  data + which;
    }
}

